<?php

namespace STUBR\Markdownvh\ViewHelpers;

/**
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */
class MdtohtmlViewHelper extends \TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper
{

    /**
     * Initializes additional arguments available for this view helper.
     */
    public function initializeArguments()
    {
        parent::initializeArguments();
        $this->registerArgument(
            'content',
            'string',
            'Markdown to render',
            true
        );
    }


    /**
     * Render markdown to html
     *
     * @return string
     */
    public function render()
    {
        $content = $this->arguments['content'];

        if (!class_exists('Parsedown')) {
            require_once(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath(
                    'markdownvh'
                ) . 'Classes/Vendor/Erusev/Parsedown/Parsedown.php');
        }

        // if you have troubles with newlines not being parsed, enable or adapt this line:
        // $content = str_replace('\n', "\n", $content);
        // explanation: newlines can't be parsed when in a single quoted string; convert them to double quoted
        // http://stackoverflow.com/questions/14710264/php-convert-single-quote-to-double-quote-for-line-break-character

        $parseDown = new \Parsedown();
        return $parseDown->text((string)$content);
    }
}
